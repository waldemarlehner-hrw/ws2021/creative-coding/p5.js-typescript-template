/**
 * @type {import('vite').UserConfig}
 */


function getBase(useGitlab) {
    if(useGitlab) {
        console.info("Building for Gitlab");
        const projectDirectory = process.env.CI_PAGES_URL;
        return projectDirectory; 
    }
    return ""
}


const config = {
    base: getBase(process.env.GITLAB_BUILD == "1") 
};
  
export default config