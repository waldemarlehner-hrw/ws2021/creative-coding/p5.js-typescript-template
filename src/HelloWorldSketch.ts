import p5 from "p5";

class HelloWorldSketch {
	
	private canvas?: p5.Renderer;
	private counter = 0;
	private speed = 0.1;
	private offset = 100;
	private screenSize = 500;

	constructor(private p: p5){
		p.setup = () => this.setup();
		p.draw = () => this.draw();
	}

	private setup() {
		this.canvas = this.p.createCanvas(this.screenSize, this.screenSize);
		this.canvas.parent("app");
	}

	private draw() {
		this.p.background("#eaeaea");
		let x = Math.sin(this.counter * this.speed) * this.offset + this.screenSize / 2;
		let y = Math.cos(this.counter * this.speed) * this.offset + this.screenSize / 2;
		this.p.ellipse(x,y, 10, 10)
		this.counter++;
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new HelloWorldSketch(p5);
	}
}

export default HelloWorldSketch;
